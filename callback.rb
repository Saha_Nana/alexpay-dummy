

CREDIT_RETURN_URL = "http://67.205.74.208:8001/creditReturn"


post '/creditReturn' do
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
 
   tr = TransactionSummary.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TransactionSmmary ---Tr #{tr}"

  if tr.to_i == 1
    
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TransactionSummary.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco,:sender_name, :voucher_code)[0]
    nw_code = res.telco
    
    puts "lets see the recipient#{res.recipient_number}and  mobile no #{res.mobile_number} and #{res.amount} and telco #{res.telco} and recipeint telco #{res.recipient_telco} and sender name #{res.sender_name} and voucher code #{res.voucher_code}"
   
    v_num =res.voucher_code
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts "***************************************"
      puts  "Success in Callback"
      puts "***************************************"
      
      strText = "You have received GHC#{res.amount} from #{res.sender_name} with mobile number #{res.recipient_number}.Your transaction id is: #{trans_ref} "
      strText2 = "You have successfully transfered GHC#{res.amount} to #{res.mobile_number}. Your transaction id is: #{trans_ref}"
      
      puts sendmsg(MSGSENDERID, res.recipient_number, strText2)
      puts sendmsg(MSGSENDERID, res.mobile_number, strText)
   
     
 
    else
     
     puts "**************Failure in Credit Callback*************************"
     puts "***************************************"
     puts "-----Mobile number is #{res.mobile_number}"
     puts "-----Recipient number is #{res.recipient_number}"
     # puts sendmsg(MSGSENDERID, res.mobile_number, STRTRANSFAILURE)
      puts sendmsg(MSGSENDERID, res.recipient_number, STRTRANSFAILURE)
     
    end
    
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     
 

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
 
end



post '/alex_pay_callback' do

  
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = TransactionSummary.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TransactionSmmary ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TransactionSummary.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :recipient_telco, :sender_name, :voucher_code)[0]
    nw_code = res.recipient_telco
    
    
    puts "lets see the callback mobile no #{res.recipient_number} and #{res.amount} and #{res.sender_name}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
     # if balance_enough?(res.amount, res.telco)
        
      if nw_code == "MTN"
        
        v_num ="n/a"
       
         puts "using MTN"
        #Thread.new {
             mobileMoneyReq(MTN_MERCHANT, res.recipient_number, res.amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,v_num,res.mobile_number,res.telco,res.sender_name)
       #}
        elsif nw_code == "VOD"
          
          puts "using vodafone"
           v_num =res.voucher_code
        #  Thread.new { 
       
            mobileMoneyReq(VOD_MERCHANT, res.recipient_number, res.amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,v_num,res.mobile_number,res.telco,res.sender_name)
        #}
        elsif nw_code == "TIG" 
        
          puts "using tigo"
         # Thread.new {
             v_num ="n/a"
       
          mobileMoneyReq(TIG_MERCHANT, res.recipient_number, res.amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,v_num,res.mobile_number,res.telco,res.sender_name)
       # }  
        elsif nw_code == "AIR"
          v_num ="n/a"
       
          puts "using airtel"
         # Thread.new {
          mobileMoneyReq(MERCHANT_NO, res.recipient_number, res.amount, STRREF, CREDIT_RETURN_URL, CLIENT_ID, res.recipient_telco, TRANS_TYPE_CREDIT,v_num,res.mobile_number,res.telco,res.sender_name)
        # }
        end
      
        
    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts sendmsg(MSGSENDERID, res.mobile_number, STRTRANSFAILURE)
     
    end
    
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     


  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
 

end


post '/virtual_accounts_callback' do
  
   request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = VirtualTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------Virtual Transaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = VirtualTransaction.where(ack_code: trans_ref).select(:amount,:mobile_number, :telco, :voucher_code)[0]
    #nw_code = res.recipient_telco
    
    
    puts "lets see the callback mobile no #{res.mobile_number} and amount  #{res.amount}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = CustomerVirtualWallet.where(mobile_number: res.mobile_number)[0] 
     puts "OLd AMOUNT is #{old_amount.amount}"
     
     new_amount = old_amount.amount + res.amount
     
       puts "NEW AMOUNT is #{new_amount}"
     
     CustomerVirtualWallet.where(mobile_number: res.mobile_number).update(:amount => new_amount)
     
 
      strText2 = "You have successfully deposited GHC#{res.amount} to your virtual account.Your new virtual account is #{new_amount}. Your transaction id is: #{trans_ref}"
      
      puts sendmsg(MSGSENDERID, res.mobile_number, strText2)
  
     

    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts sendmsg(MSGSENDERID, res.mobile_number, STRTRANSFAILURE)
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
  
  
end

post '/w_virtual_accounts_callback' do
  
   request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = VirtualTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------Virtual Transaction ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = VirtualTransaction.where(ack_code: trans_ref).select(:amount,:mobile_number, :telco, :voucher_code)[0]
    #nw_code = res.recipient_telco
    
    
    puts "lets see the callback mobile no #{res.mobile_number} and amount  #{res.amount}"
   
  
    
 if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = CustomerVirtualWallet.where(mobile_number: res.mobile_number)[0] 
     puts "OLd AMOUNT is #{old_amount.amount}"
     puts "VIrtual Wallet balance is #{old_amount.amount} "
     if old_amount.amount == 0.00 
        strText3 = "Your virtual wallet balance is insufficient to withdraw. Please top-up. "
      
      puts sendmsg(MSGSENDERID, res.mobile_number, strText3)
     else
     
     new_amount = old_amount.amount - res.amount
     
       puts "NEW AMOUNT is #{new_amount}"
     
     CustomerVirtualWallet.where(mobile_number: res.mobile_number).update(:amount => new_amount)
    
      strText2 = "You have successfully withdrawn GHC#{res.amount} to your mobile money account.Your new virtual account balance is #{new_amount}. Your transaction id is: #{trans_ref}"
      
      puts sendmsg(MSGSENDERID, res.mobile_number, strText2)
  
     end

    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts sendmsg(MSGSENDERID, res.mobile_number, STRTRANSFAILURE)
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
  
  
end




post '/third_party_callback' do

  
  request.body.rewind
  req = JSON.parse request.body.read
  puts req.inspect

  trans_id = req['trans_id']
  trans_status = req['trans_status']
  trans_ref = req['trans_ref']
  message = req['message']

  puts '---------PARAMETERS-------'
  puts trans_id.to_s
  puts trans_status.to_s
  puts trans_ref.to_s
  puts message.to_s

  trans_split = trans_status.split('/')
  trans_state = trans_split[0]
  network_state = trans_split[1]
  time = Time.new
  _updatetime = time.strftime('%y%m%d%H%M%S%L')
  
  
   tr = TVirtualTransaction.where(ack_code: trans_ref).select(:ack_code).count
   
   
   puts "---------------TransactionSmmary ---Tr #{tr}"

  if tr.to_i == 1
    
    puts "-------Got Here-------"
  
    
    savePaystate(network_state, trans_id, trans_ref, message, trans_state)

    res = TVirtualTransaction.where(ack_code: trans_ref).select(:amount, :recipient_number, :mobile_number, :telco, :sender_name, :voucher_code)[0]
 
    
    
    puts "lets see the callback mobile no #{res.recipient_number} and #{res.amount} and #{res.sender_name}"
   
  
    
  if (network_state.to_s == '200') || (trans_state.to_s == '000')

      puts  "Success in Callback"
      
      
     old_amount = TVirtualTransaction.where(mobile_number: res.mobile_number)[0] 
     puts "OLd AMOUNT is #{old_amount.amount}"
     puts "VIrtual Wallet balance is #{old_amount.amount} "
     if old_amount.amount == 0.00 
        strText3 = "Your virtual wallet balance is insufficient to withdraw. Please top-up. "
      
      puts sendmsg(MSGSENDERID, res.mobile_number, strText3)
     else
     
     new_amount = old_amount.amount + res.amount
     
       puts "NEW AMOUNT is #{new_amount}"
     
     TVirtualTransaction.where(mobile_number: res.mobile_number).update(:amount => new_amount)
    
      strText2 = "You have successfully withdrawn GHC#{res.amount} to your mobile money account. Your transaction id is: #{trans_ref}"
      
      puts sendmsg(MSGSENDERID, res.mobile_number, strText2)
  
     end

    else 
     
     puts "**************Failure in Callback*************************"
     puts "***************************************8"
      puts sendmsg(MSGSENDERID, res.mobile_number, STRTRANSFAILURE)
     
    end
      resp = { resp_code: '00', resp_desc: 'Success' }
  else
    resp = { resp_code: '01', resp_desc: 'Failure' }
  end
     

  puts "----Final  Response--------"
  puts resp.to_json
  puts
  return resp.to_json
  
  
  
end


