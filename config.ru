require 'rubygems'
require 'sinatra'

set :environment, ENV['RACK_ENV'].to_sym
disable :run, :reload

# require './main_app'
require './main_app_backup'

run Sinatra::Application
