require 'sinatra'
require 'json'
require 'nokogiri'
require 'sinatra/activerecord'
require 'yaml'
require 'faraday'
require 'net/http'
require 'uri'
require 'savon'
require 'digest/md5'
require 'openssl'
require './model'
require './functions'
require './callback'


######## CONSTANTS ###############

MAINMENU="Welcome to AlexPay \n\n 1.Transfer Funds\n\n 2.My Profile"
SUB="Welcome to AlexPay \n\n1.Subscribe"
CHANGEPIN ="1.Change Pin"
PINMSG = "You have successfully changed your pin"
DEFAULT_PIN ="1234"
MAX_PIN = 4
SUBMSG="You have successfully subscribed! Default PIN is 1234.Please go to 'My profile' and change your pin."
TELCOSELECT="Select network \n\n1.Tigo\n2.MTN\n3.Vodafone\n4.Airtel"
VODMSG= "You need a voucher code in order to make payment. Please dial *110# and select 6 to generate a voucher code and return here to continue.\n\n1. Continue\n 2. Exit\n"
ENTERAMNT = "Please enter an amount to deposit"
ID_TYPE = "Please select an ID type \n\n 1. Voter's ID\n\n 2. Passport \n\n 3.Driver's License"
MSGSENDERID = "AlexPay"
STRTRANSFAILURE ="Sorry,your transaction failed"

INVALIDSELECTION = "Sorry. Wrong selection. Please try again."
MERCHANT_NUMBER = "261064828"
NETWRK = "AIR"
TRANS_TYPE_DEBIT ="DR"
TRANS_TYPE_CREDIT ="CR"
CALLBACK_URL = "http://67.205.74.208:8001/alex_pay_callback"
VIR_CALLBACK_URL = "http://67.205.74.208:8001/virtual_accounts_callback"
W_VIR_CALLBACK_URL = "http://67.205.74.208:8001/w_virtual_accounts_callback" 
THIRD_PARTY_CALLBACK ="http://67.205.74.208:8001/third_party_callback" 
CLIENT_ID = 22
SECRET_KEY ='W0Dz7jXYrkS3QJr5RpnwZGjY8i5PxzuHXhzmiMfLdnM9BePz4RDRPoQibIJ4OLxIOD2UUpRTujcGfTA4dB3Qtw=='
CLIENT_KEY = 'XlnBgzaj/4ZRseV3AratiyjVJeR75Hbhy0wq3y2XIO2bPNZ6jAct5XIC70oovj3kJf4oygF6kcgIzAbbwc7kvw=='
MERCHANT_NO = "261064828"
TIG_MERCHANT = "0271300376"
MTN_MERCHANT = ""
VOD_MERCHANT = ""
# STRREF = "AppsNmobile"

post '/' do
  request.body.rewind
  @payload=JSON.parse(request.body.read)

  json_vals = @payload

  msg_type = json_vals['msg_type']
  sessionID=json_vals['session_id']
  service_key=json_vals['service_code'] 
  mobile_number=json_vals['msisdn']
  ussd_body=json_vals['ussd_body'].strip if json_vals.has_key?('ussd_body')
  nw_code = json_vals['nw_code']
  first_name = json_vals['first_name']
  last_name = json_vals['last_name']
  alt_mobile_number = json_vals['alt_mobile_number']

  if msg_type == "0"
    sequence = "0"
  else
    sequence=get_sequence(sessionID,mobile_number);
  end
  
  log_book(msg_type,service_key,sessionID,sequence,mobile_number,ussd_body,nw_code)
  process(msg_type,service_key,sessionID,sequence,mobile_number,ussd_body,nw_code,first_name,last_name,alt_mobile_number)

end

def log_book(msg_type,service_key,sessionID,sequence,mobile_number,ussd_body,nw_code)

  n = UssdLog.new 
  n.msg_type = msg_type
  n.service_code = service_key
  n.session_id = sessionID 
  n.msisdn = mobile_number
  n.ussd_body = ussd_body
  n.nw_code = nw_code
  n.sequence = sequence
  n.save
end

####### USSD PROCESS ########### 

def process(msg_type, service_key, sessionID,sequence ,mobile, ussd_body,nw_code,first_name,last_name,alt_mobile_number)
  s_params=Hash.new

  s_params['msg_type']=msg_type
  s_params['session_id']=sessionID
  s_params['service_code']=service_key
  s_params['msisdn']=mobile
  s_params['nw_code']=nw_code

  subscribed_number = Subscription.where(:msisdn=>mobile).exists?
  if subscribed_number == true
    if sequence == "0"
    
    if nw_code == "02"

        s_params['msg_type']= '1' 
        s_params['ussd_body']=VODMSG
      
    else
      
        strConfirm = ussd_body.strip

        s_params['msg_type']= '1'
        s_params['ussd_body']=MAINMENU

        p s_params.to_json
      end  
 
  elsif sequence=="1"

      strReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '1').select(:ussd_body)[1][:ussd_body]

      puts "-------------------"
      puts "------StrREPLY in sequence1 is #{strReply}"
      puts "----------------------"

     if nw_code == "02"

        if ussd_body == "1"

          s_params['msg_type']= '1'
          s_params['ussd_body']=MAINMENU

        elsif ussd_body =="2"

          s_params['msg_type']= '2'
          s_params['ussd_body']="you chose to exit"
        end
     else

        if strReply == "1"

          ussdTracker(sessionID, service_key, mobile, ussd_body, 1)

          s_params['msg_type']= '1'
          s_params['ussd_body']="1.Transfer to virtual Account\n\n2. Transfer to Networks" 

        elsif strReply == "2"
          ussdTracker(sessionID, service_key, mobile, ussd_body, 1)

          s_params['msg_type']= '1'
          s_params['ussd_body']= CHANGEPIN

        end
  end

    elsif sequence=="2"

      
        strReply = UssdLog.where(' session_id=? and msisdn =? and sequence=?',

      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      
         puts "StrREPLY in sequence2 is #{strReply}"
      
      vstrReply = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[1][:ussd_body]

      puts "-------------------"
      puts "------StrREPLY in sequence2 is #{strReply}"
      puts "------vstrReply in sequence2 is #{vstrReply}"

      puts "----------------------"
      
          if nw_code == "02"
           if vstrReply == "1"

          ussdTracker(sessionID, service_key, mobile, ussd_body, 1)

          s_params['msg_type']= '1'
          s_params['ussd_body']="1.Transfer to virtual Account\n\n2. Transfer to Networks" 

        elsif vstrReply == "2"
          ussdTracker(sessionID, service_key, mobile, ussd_body, 1)

          s_params['msg_type']= '1'
          s_params['ussd_body']= CHANGEPIN

        end
        
        else  
            
          

        if strReply == "1"
              if ussd_body == '1'
                
                 s_params['msg_type']= '1'
          s_params['ussd_body']="Virtual Wallet Account\n\n1.Deposit \n\n2. Withdraw"  ## for virtual networks
              
              elsif ussd_body == '2'  
                
                 s_params['msg_type']= '1'
          s_params['ussd_body']="Please Enter your Pin to Continue"  ## for network transfer
                
              end
        
        elsif (strReply =='2' and ussd_body == '1')

          ussdTracker(sessionID, service_key, mobile, ussd_body, 2)

          s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter old pin"               ## changing pin part

        end
end
    
    elsif sequence=="3"

      strReply = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
    
      vstrReply = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
    
      prev_ussd_body = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
      puts "-------------------"
      puts "------SEQUENCE 3 ----- StrREPLY is #{strReply}"
       puts "------PRV USSD_BODY IN SEQ 3 ----- prev_ussd_body is #{prev_ussd_body}"
  
      puts "----------------------"

     if nw_code == "02"
       
        if vstrReply == "1"
              if ussd_body == '1'
                
                 s_params['msg_type']= '1'
          s_params['ussd_body']="Virtual Wallet Account\n\n1.Deposit \n\n2. Withdraw"  ## for virtual networks
              
              elsif ussd_body == '2'  
                
                 s_params['msg_type']= '1'
          s_params['ussd_body']="Please Enter your Pin to Continue"  ## for network transfer
                
              end
        
        elsif (vstrReply =='2' and ussd_body == '1')

          ussdTracker(sessionID, service_key, mobile, ussd_body, 2)

          s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter old pin"               ## changing pin part

        end
        
        else  
     
  
        if   (strReply =='1' and prev_ussd_body == '1')
          
         if ussd_body == '1'
           s_params['msg_type']= '1'
          s_params['ussd_body']= "Deposit \n\n1.Self \n\n2. Third-Party"
          elsif ussd_body == '2'
            s_params['msg_type']= '1'
          s_params['ussd_body']= "Withdraw\n\n1.Self \n\n2. Third-Party"
         end
          
        elsif (strReply =='1' and prev_ussd_body == '2')
          
         old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin
          
          default_pin = Subscription.where(' msisdn=? and status=?',mobile,0).order('id desc')[0].pin

          old_pin2 = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
           #new_pin = UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2} and default pin is #{default_pin}"
          puts "----------------"
          if old_pin2.is_number?
            pin = validate_pin(old_pin2)
            if pin == "success"
              if pin_hash(old_pin2) == default_pin
      
                # s_params['msg_type']= '2'
                # s_params['ussd_body']="Please Change Your default Pin before you continue.Go to MyProfile to change."
#                
               # elsif pin_hash(old_pin2) == old_pin
                ussdTracker(sessionID, service_key, mobile, ussd_body, 2)

                s_params['msg_type']= '2' 
                s_params['ussd_body']="Please Change Your default Pin before you continue.Go to MyProfile to change."
                
              elsif pin_hash(old_pin2) == old_pin
                ussdTracker(sessionID, service_key, mobile, ussd_body, 2)

                s_params['msg_type']= '1'
                s_params['ussd_body']=TELCOSELECT

              else

                s_params['msg_type']= '2'
                s_params['ussd_body']="incorrect pin. Cant procceed. Try again"
              end
            elsif pin == "periodfail" || pin == "period_fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Dots are not allowed in the pin number"
            elsif pin == "fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Your pin number is supposed to be 4 digits. Please check and try again."
            end

          else
            s_params['msg_type'] = "2"
            s_params['ussd_body'] = "Not number"
          end 

     

        elsif strReply =='2'

          old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin

          old_pin2 = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body

          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2}"
          puts "----------------"
          if pin_hash(old_pin2) == old_pin

            ussdTracker(sessionID, service_key, mobile, ussd_body, 4)

            s_params['msg_type']= '1'
            s_params['ussd_body']= " Enter  new pin"

          else
            s_params['msg_type']= '2'
            s_params['ussd_body']= " Old pin incorrect. Try again"
          end
        end

end

    elsif sequence == "4"

      strReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      
      vstrReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
      
     prev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
      vprev_ussd_body = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body2 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
       puts "-------------------"
      puts "------SEQUENCE 4 ----- StrREPLY is #{strReply}"
       puts "------SEQUENCE 4 ----- vstrReply is #{vstrReply}"
      puts "------SEQUENCE 4 ----- prev_ussd_body1 is #{prev_ussd_body1}"
       puts "------SEQUENCE 4 ----- prev_ussd_body1 is #{prev_ussd_body2}"
       puts "------SEQUENCE 4 ----- prev_ussd_body is #{prev_ussd_body}"
        puts "------SEQUENCE 4 ----- vprev_ussd_body is #{vprev_ussd_body}"
      puts "----------------------"

       if nw_code == "02"
         
          if   (vstrReply =='1' and vprev_ussd_body == '1')
          
         if ussd_body == '1'
           s_params['msg_type']= '1'
          s_params['ussd_body']= "Deposit\n\n1.Self \n\n2. Third-Party"
          elsif ussd_body == '2'
            s_params['msg_type']= '1'
          s_params['ussd_body']= "Withdraw\n\n1.Self \n\n2. Third-Party"
         end
          
        elsif (vstrReply =='1' and vprev_ussd_body == '2')
          
         old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin
          
          default_pin = Subscription.where(' msisdn=? and status=?',mobile,0).order('id desc')[0].pin

          old_pin2 = UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
           #new_pin = UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2} and default pin is #{default_pin}"
          puts "----------------"
          if old_pin2.is_number?
            pin = validate_pin(old_pin2)
            if pin == "success"
              if pin_hash(old_pin2) == default_pin
      
                # s_params['msg_type']= '2'
                # s_params['ussd_body']="Please Change Your default Pin before you continue.Go to MyProfile to change."
#                
               # elsif pin_hash(old_pin2) == old_pin
                ussdTracker(sessionID, service_key, mobile, ussd_body, 2)

                s_params['msg_type']= '2' 
                s_params['ussd_body']="Please Change Your default Pin before you continue.Go to MyProfile to change."
                
              elsif pin_hash(old_pin2) == old_pin
                ussdTracker(sessionID, service_key, mobile, ussd_body, 2)

                s_params['msg_type']= '1'
                s_params['ussd_body']=TELCOSELECT

              else

                s_params['msg_type']= '2'
                s_params['ussd_body']="incorrect pin. Cant procceed. Try again"
              end
            elsif pin == "periodfail" || pin == "period_fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Dots are not allowed in the pin number"
            elsif pin == "fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Your pin number is supposed to be 4 digits. Please check and try again."
            end

          else
            s_params['msg_type'] = "2"
            s_params['ussd_body'] = "Not number"
          end 

     

        elsif vstrReply =='2'

          old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin

          old_pin2 = UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body

          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2}"
          puts "----------------"
          if pin_hash(old_pin2) == old_pin

            ussdTracker(sessionID, service_key, mobile, ussd_body, 4)

            s_params['msg_type']= '1'
            s_params['ussd_body']= " Enter  new pin"

          else
            s_params['msg_type']= '2'
            s_params['ussd_body']= " Old pin incorrect. Try again"
          end
        end
         
         
       else

        if  (strReply =='1' and prev_ussd_body1 == '1')

          if ussd_body == '1'
          s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to deposit(self) "
          elsif ussd_body == '2'
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter recipient phone number to deposit"
          end
       elsif  (strReply =='1' and prev_ussd_body1 == '2')
         
          if ussd_body == '1'
          s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to withdraw(self) "
          elsif ussd_body == '2'
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter recipient phone number to withdraw"
          end
          
           elsif  (strReply =='1' and prev_ussd_body2 == '2')
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter recipient phone number"

        elsif strReply =='2'

          new_pin = UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
          recipient_number = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
          puts "----------------"
          puts "New pin is #{new_pin} "
          
           if new_pin.is_number?
            pin = validate_pin(new_pin) 
            if pin == "success"

          pin_save2(sessionID, mobile,new_pin)

          strText2 ="Alert! Your pin has been changed. Your new pin is #{new_pin} "
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

          ussdTracker(sessionID, service_key, mobile, ussd_body, 4)

          s_params['msg_type']= '2'
          s_params['ussd_body']= PINMSG
          
          elsif pin == "periodfail" || pin == "period_fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Dots are not allowed in the pin number"
            elsif pin == "fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Your pin number is supposed to be 4 digits. Please check and try again."
            end
          
          else
            s_params['msg_type'] = "2"
            s_params['ussd_body'] = "Not number"
          end 
          

        end
end

    elsif sequence == '5'
      strReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      
      vstrReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
      
     prev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body0 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body2 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
      main = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
       puts "-------------------"
      puts "------SEQUENCE 5 ----- StrREPLY is #{strReply}"
         puts "------SEQUENCE 5 ----- vStrREPLY is #{vstrReply}"
      puts "------SEQUENCE 5 ----- prev_ussd_body1 is #{prev_ussd_body1}"
         puts "------SEQUENCE 5 ----- prev_ussd_body0 is #{prev_ussd_body0}"
       puts "------SEQUENCE 5 ----- prev_ussd_body2 is #{prev_ussd_body2}"
       puts "------main ----- prev_ussd_body2 is #{main}"
      puts "----------------------"
      
      if nw_code == "02"
          if  (vstrReply =='1' and prev_ussd_body1 == '1')

          if ussd_body == '1'
          s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to deposit(self) "
          elsif ussd_body == '2'
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter recipient phone number to deposit"
          end
       elsif  (vstrReply =='1' and prev_ussd_body1 == '2')
         
          if ussd_body == '1'
          s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to withdraw(self) "
          elsif ussd_body == '2'
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter recipient phone number to withdraw"
          end
          
           elsif  (vstrReply =='1' and prev_ussd_body2 == '2')
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter recipient phone number"

        elsif vstrReply =='2'

          new_pin = UssdLog.where(' session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
          recipient_number = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
          puts "----------------"
          puts "New pin is #{new_pin} "
          
           if new_pin.is_number?
            pin = validate_pin(new_pin) 
            if pin == "success"

          pin_save2(sessionID, mobile,new_pin)

          strText2 ="Alert! Your pin has been changed. Your new pin is #{new_pin} "
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

          ussdTracker(sessionID, service_key, mobile, ussd_body, 4)

          s_params['msg_type']= '2'
          s_params['ussd_body']= PINMSG
          
          elsif pin == "periodfail" || pin == "period_fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Dots are not allowed in the pin number"
            elsif pin == "fail"
              s_params['msg_type'] = "2"
              s_params['ussd_body'] = "Your pin number is supposed to be 4 digits. Please check and try again."
            end
          
          else
            s_params['msg_type'] = "2"
            s_params['ussd_body'] = "Not number"
          end 
          

        end
        
      else
      
     if  strReply =='1'  

         if (prev_ussd_body0 == '1' and prev_ussd_body1 == '1' )
          s_params['msg_type']= '1'
          s_params['ussd_body']= self_dep_summary(sessionID, mobile,nw_code)
         elsif (prev_ussd_body0 == '1' and prev_ussd_body1 == '2' )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to deposit " 
         
          elsif  ( prev_ussd_body0 == '2' and  prev_ussd_body1 == '1' )
         
     
          s_params['msg_type']= '1'
          s_params['ussd_body']= self_with_summary(sessionID, mobile,nw_code)
    
        elsif   ( prev_ussd_body0 == '2' and  prev_ussd_body1 == '2' )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to withdraw "
          
         
         elsif   main == '2'  
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount"
        end
      end
          
  end        
          
  elsif sequence == '6'
      strReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      
       vstrReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
       main = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
      
     prev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      
        vprev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '6').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body0 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body2 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
      old_pin = PinTable.where(' msisdn=? and alt_status=?',mobile,0).order('id desc')[0].pin
          
       

          old_pin2 = UssdLog.where(' session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
           #new_pin = UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
          puts "----------------"
          puts "----------------"
          puts "Old pin is #{old_pin} and old pin entered is #{old_pin2} "
     
      puts "------SEQUENCE 6 ----- prev_ussd_body1 is #{prev_ussd_body1}"
         puts "------SEQUENCE 6 ----- prev_ussd_body0 is #{prev_ussd_body0}"
       puts "------SEQUENCE 6 ----- vprev_ussd_body1 is #{vprev_ussd_body1}"
       puts "------SEQUENCE 6 ----- prev_ussd_body2 is #{prev_ussd_body2}"
        puts "------main ----- prev_ussd_body2 is #{main}"
      puts "----------------------"
      
     if nw_code == "02"
         if  vstrReply =='1'  

         if (prev_ussd_body0 == '1' and vprev_ussd_body1 == '1' and prev_ussd_body1 == '1' ) 
          s_params['msg_type']= '1'
          s_params['ussd_body']= vself_dep_summary(sessionID, mobile,nw_code)
         elsif (prev_ussd_body0 == '1' and vprev_ussd_body1 == '2' and prev_ussd_body1 == '1'  )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to deposit "
         
          elsif  ( prev_ussd_body1 == '2' and  vprev_ussd_body1 == '1' )
         
     
          s_params['msg_type']= '1'
          s_params['ussd_body']= vself_with_summary(sessionID, mobile,nw_code)
    
        elsif   ( prev_ussd_body0 == '1' and prev_ussd_body1 == '2' and  vprev_ussd_body1 == '2' )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount to withdraw"
          
         
         elsif   (prev_ussd_body2 == '2' and pin_hash(old_pin2) == old_pin)
             s_params['msg_type']= '1'
          s_params['ussd_body']= " Enter amount"
        end
      end
       
     else 
     if  strReply =='1'  

         if (prev_ussd_body0 == '1' and prev_ussd_body1 == '1' )
          s_params['msg_type']= '2'
          s_params['ussd_body']= virtualprocessPayment(ussd_body, sessionID, mobile, sequence,nw_code)
   
         elsif (prev_ussd_body0 == '1' and prev_ussd_body1 == '2' )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= thirdparty_dep_summary(sessionID, mobile,nw_code)
         
          elsif  ( prev_ussd_body0 == '2' and  prev_ussd_body1 == '1' )
         
     
          s_params['msg_type']= '2'
          s_params['ussd_body']= virtualprocessPayment2(ussd_body, sessionID, mobile, sequence,nw_code)
    
        elsif   ( prev_ussd_body0 == '2' and  prev_ussd_body1 == '2' )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= thirdparty_withdrawal_summary(sessionID, mobile,nw_code)
           
           elsif   main == '2'
             s_params['msg_type']= '1'
          s_params['ussd_body']= previewEntries(sessionID, mobile,nw_code)
          
          end
       end  
          
     end     
      elsif sequence == '7'
      strReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      
      
       vstrReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
       main = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
     prev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      
       vprev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '6').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body0 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body2 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       puts "-------------------"
      puts "------SEQUENCE  ----- StrREPLY is #{strReply}"
       puts "------SEQUENCE  ----- vStrREPLY is #{vstrReply}"
      puts "------SEQUENCE 7 ----- prev_ussd_body1 is #{prev_ussd_body1}"
         puts "------SEQUENCE 7 ----- prev_ussd_body0 is #{prev_ussd_body0}"
       puts "------SEQUENCE 7 ----- prev_ussd_body2 is #{prev_ussd_body2}"
        puts "----- main----- main is #{main}"
      puts "----------------------"
      
     if nw_code == "02" 
       
      if  vstrReply =='1'  

         if (prev_ussd_body0 == '1' and vprev_ussd_body1 == '1' and prev_ussd_body1 == '1' )  
          s_params['msg_type']= '2'
          s_params['ussd_body']= " Deposit Complete (self)"
         elsif (prev_ussd_body0 == '1' and vprev_ussd_body1 == '2' and prev_ussd_body1 == '1'  )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= vthirdparty_dep_summary(sessionID, mobile,nw_code)
         
          elsif  ( prev_ussd_body0 == '1' and  vprev_ussd_body1 == '1'  and  prev_ussd_body1 == '2') 
         
     
          s_params['msg_type']= '2'
          s_params['ussd_body']= " Withdrawal Complete (self)"
    
        elsif   ( prev_ussd_body1 == '2' and  vprev_ussd_body1 == '2' )
            
             s_params['msg_type']= '1'
          s_params['ussd_body']= vthirdparty_withdrawal_summary(sessionID, mobile,nw_code)
           
           elsif   prev_ussd_body2 == '2'
             s_params['msg_type']= '1'
          s_params['ussd_body']= "Enter voucher code"
          
          end
       end  
    else
     if  strReply =='1'  

        if (prev_ussd_body0 == '1' and prev_ussd_body1 == '2' )
            
             s_params['msg_type']= '2'
          s_params['ussd_body']= third_party_processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
         
         
    
        elsif   ( prev_ussd_body0 == '2' and  prev_ussd_body1 == '2' )
            
             s_params['msg_type']= '2'
          s_params['ussd_body']= " Wihtdrawal Complete (thirdparty)"
          
           elsif   main == '2'
             s_params['msg_type']= '2'
          s_params['ussd_body']= processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
           end
       end  
 

 end
 
        elsif sequence == '8'
          
          strReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      
      
       vstrReply = UssdLog.where('session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]
      
      
     prev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      
       vprev_ussd_body1 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '6').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body0 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       prev_ussd_body2 = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
      
       puts "-------------------"
      puts "------SEQUENCE  ----- StrREPLY is #{strReply}"
       puts "------SEQUENCE  ----- vStrREPLY is #{vstrReply}"
      puts "------SEQUENCE 8 ----- prev_ussd_body1 is #{prev_ussd_body1}"
         puts "------SEQUENCE 8 ----- prev_ussd_body0 is #{prev_ussd_body0}"
       puts "------SEQUENCE 8 ----- prev_ussd_body2 is #{prev_ussd_body2}"
      puts "----------------------"
          
          
          if nw_code == "02" 
            
             if  vstrReply =='1'  

        if (prev_ussd_body0 == '1' and vprev_ussd_body1 == '2' and prev_ussd_body1 == '1')
            
             s_params['msg_type']= '2'
          s_params['ussd_body']= " Deposit Complete (thirdparty)"
         
         
    
        elsif   ( prev_ussd_body1 == '2' and  vprev_ussd_body1 == '2' )
            
             s_params['msg_type']= '2'
          s_params['ussd_body']= " Withdrawal Complete (thirdparty)"
          
           elsif   prev_ussd_body2 == '2'
             s_params['msg_type']= '1'
          s_params['ussd_body']=previewEntries2(sessionID, mobile,nw_code)
          
           v_processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
           end
       end 
       
    end
    
    
    elsif sequence == '9'
       s_params['msg_type']= '2'
          s_params['ussd_body']=  v_processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
  end 
            
          
          
          
 ### THis is the subscription part
  else
      selectedOption = ussd_body
    if sequence == "0"

      s_params['msg_type']= '1'
      s_params['ussd_body']= SUB

    elsif sequence == "1"

      s_params['msg_type']= '1'
      s_params['ussd_body']= "Please Enter First Name"

    elsif sequence == "2"
      s_params['msg_type']= '1'
      s_params['ussd_body']= "Please Enter Last Name"
      
      
    elsif sequence == "3"
      s_params['msg_type']= '1'
      s_params['ussd_body']= "Do you have any alternative number?\n\n1.Yes\n2.No"
    elsif sequence == "4"
       alt = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '4').select(:ussd_body)[1][:ussd_body]
       
       if alt == "1"
      
      s_params['msg_type']= '1'
      s_params['ussd_body']= "Please Enter Alternate Number"
      else 
        
         s_params['msg_type']= '1'
      s_params['ussd_body']= ID_TYPE 
      end
        
  
    elsif sequence == "5"
      alt = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      if alt == "1"
      s_params['msg_type']= '1'
      s_params['ussd_body']= ID_TYPE 
      else
        s_params['msg_type']= '1'
      s_params['ussd_body']= "Enter ID number"  
        
      end
     elsif sequence == "6"
        alt = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      if alt == "1"
      s_params['msg_type']= '1'
      s_params['ussd_body']= "Enter ID number" 
      else
        s_params['msg_type']= '1'
      s_params['ussd_body']= previewSubscription_new(sessionID, mobile,nw_code)
         
    end
    elsif sequence == "7"
       alt = UssdLog.where(' session_id=? and msisdn =? and sequence=?',
      sessionID, mobile, '5').select(:ussd_body)[0][:ussd_body]
      if alt == "1"
      s_params['msg_type']= '1'
      s_params['ussd_body']= previewSubscription(sessionID, mobile,nw_code)
      else
         s_params['msg_type']= '2'
      s_params['ussd_body']= SUBMSG 
      
      
      strText2 ="You have successfully subscribed! Default PIN is 1234.Please go to 'My profile' and change your pin."
       recipient_number = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]   
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

      first_name = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
      last_name= UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
      #alt_mobile_number= UssdLog.where(' session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
      id_type = id_type1(sessionID) 
      id_num= UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id asc')[0].ussd_body 
      _sub= subscriber_id
      subscriber(sessionID, mobile, first_name,last_name,"",_sub,pin_hash(DEFAULT_PIN),id_type,id_num)
      pin_save(sessionID, mobile,DEFAULT_PIN,_sub)  
        amount = 0.00 
      virtual_wallet(mobile,amount)
     end   
    elsif sequence == "8"
      
      s_params['msg_type']= '2'
      s_params['ussd_body']= SUBMSG 
      
       strText2 ="You have successfully subscribed! Default PIN is 1234.Please go to 'My profile' and change your pin."
       recipient_number = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]   
          puts sendmsg(MSGSENDERID, recipient_number, strText2)

      first_name = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
      last_name= UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
      alt_mobile_number= UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
      id_type = id_type(sessionID) 
      id_num= UssdLog.where(' session_id=?  and sequence=?',sessionID, '8').order('id asc')[0].ussd_body 
      _sub= subscriber_id
      subscriber(sessionID, mobile, first_name,last_name,alt_mobile_number,_sub,pin_hash(DEFAULT_PIN),id_type,id_num)
      pin_save(sessionID, mobile,DEFAULT_PIN,_sub) 
      amount = 0.00 
      virtual_wallet(mobile,amount)
      
    
    end
  end

  p s_params.to_json

end

