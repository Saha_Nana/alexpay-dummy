STROPENTIMEOUT="180"

URL='http://184.173.139.74:8198'
HEADER={'Content-Type'=>'Application/json','timeout'=>'180'}

STRREF = "ALEX_PAY"
new_HEADERS = {'Content-Type'=> 'application/json','timeout'=>STROPENTIMEOUT, 'open_timeout'=>STROPENTIMEOUT}
REQHDR = {'Content-Type'=>'Application/json','timeout'=>'180'}

 
#### Generating sequence ###
def get_sequence(sessionID, mobile_number)
  seq_obj = UssdLog.where('session_id=? and msisdn=?', sessionID, mobile_number).order('id desc').limit(1)
  p "THE SEQUENCE LIST: #{seq_obj.inspect}"
  seq_num = seq_obj[0].sequence
  new_seq = seq_num.to_i + 1
  seq_obj[0].sequence = new_seq
  seq_obj[0].save
  new_seq.to_s
  
end 

def saveUSSD(msg_type, service_key, sessionID, sequence, mobile_number, ussd_body, _nw_code)
  rec = UssdLog.create(mobile_number: mobile_number, sequence: sequence, msg_type: msg_type,
                           session_id: sessionID, service_key: service_key, ussd_body: ussd_body)
end


def balance_enough?(amount, network)
    resp = check_wallet_balance
    bal = resp["balance"][network]["Withdrawal"].split[1].to_f

  puts "THE BAL IS... #{bal}"
  puts "THE BAL IS... #{network}"

    bal > amount.to_f
end

class String
  def valid_float?
    !!Float(self) rescue false
  end

 def is_number?
    true if Float(self) rescue false
  end

 def numeric?
    return true if self =~ /\A\d+\Z/
    true if Float(self) rescue false
  end

end

def validate_pin(pin)
  bool = ""

 if pin.length == MAX_PIN
    if pin.include?(".")
      bool = "periodfail"
    else
      bool = "success"
    end
  elsif pin.include?(".")
    bool = "period_fail"

 else
    bool = "fail"
  end

 return bool
end



def check_wallet_balance
    url='https://184.173.139.74:8215'
    url_endpoint='/check_wallet_bal'
    time = Time.now.strftime("%Y-%m-%d %H:%M:%S")

    begin

        con = Faraday.new(:url => url, :headers => REQHDR, :ssl => {:verify => false}) do |faraday|
            faraday.response :logger # log requests to STDOUT
            faraday.adapter Faraday.default_adapter # make requests with Net::HTTP
        end

        params = {
            :client_id => CLIENT_ID,
            :ts => time
        }

        json_params = JSON.generate(params)
        string_json = "#{json_params}"
        
        def computeSignature(secret, data)
      digest=OpenSSL::Digest.new('sha256')
        signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
      return signature
    end

        signature = computeSignature(SECRET_KEY, string_json)
        result = con.post do |request|
            request.url url_endpoint
            request.options.timeout = 180 # open/read timeout in seconds
            request.options.open_timeout = 180 # connection open timeout in seconds
            request["Authorization"]="#{CLIENT_KEY}:#{signature}"
            request.body = json_params
        end

        if result.status.to_s == "200"

            puts
            puts "::::::::::::::::::::::::::::::----  STATUS OK...  ----::::::::::::::::::::::::::::::::"
            puts

        end
        puts
        puts "Response: #{result.status} :: #{result.body}"
        puts
        return JSON.parse(result.body)

    rescue Faraday::SSLError
        puts
        puts "There was an issue sending the https request...."
        puts
    rescue Faraday::TimeoutError
        puts "Connection timeout error."

    rescue Exception => e
        puts "Error:"
        puts e.message
    end

end

##### striping off mobile numbers
def break_number(wildcard_search)
  
  if wildcard_search[0..2]=='233'
      wildcard_search="#{wildcard_search[3..wildcard_search.length]}"
   elsif wildcard_search[0]=='0' && wildcard_search.length==10
      wildcard_search="#{wildcard_search[1..wildcard_search.length]}"
   elsif wildcard_search[0]=='+' && wildcard_search[1..3]=='233'&& wildcard_search[4..wildcard_search.length]
      wildcard_search="#{wildcard_search[4..wildcard_search.length]}"
   elsif wildcard_search[0]!="+" && wildcard_search[0..2]!='233' && wildcard_search.length==9
      wildcard_search="#{wildcard_search[0..wildcard_search.length]}"
   end
   
   return wildcard_search
end


def phone_formatter(number)
             #changes phone number format to match 233247876554
    if number[0] == '0'
        num = number[1..number.size]
        "233"+num

   elsif number[0] == '+'
        number[1..number.size]

   elsif number[0] == '2'
        number
    else
        false
    end

end

def savePaystate(trans_info, trans_id, trans_ref, trans_msg, amfp_status)
  PaymentState.create!(network_status: trans_info, trans_id: trans_id, trans_ref: trans_ref,
                       trans_msg: trans_msg, amfp_status: amfp_status, status: 1)
end

def ussdTracker(session_id, service_key, mobile_number, ussd_body, level)
  UssdReqTracker.create(mobile_number: mobile_number, session_id: session_id, service_key: service_key, ussd_body: ussd_body, req_level: level)
end

def subscriber(session_id, msisdn, first_name,last_name,alt_mobile_number,subsciber_code,pin,id_type,id_num)
  Subscription.create(msisdn: msisdn, session_id: session_id, first_name: first_name, last_name: last_name, alt_mobile_number: alt_mobile_number, subsciber_code: subsciber_code,pin: pin_hash(DEFAULT_PIN),status:0,alt_status:1,id_type: id_type, id_num: id_num)
end


def virtual_wallet( mobile_number,amount)
  CustomerVirtualWallet.create(mobile_number: mobile_number,amount: 0.00,status:1,alt_status:0)
end


def pin_save(session_id, msisdn,pin,subscription_id)
  PinTable.create(
                     session_id: session_id,
                     pin: pin_hash(DEFAULT_PIN),
                     msisdn: msisdn,
                     subscription_id: subscription_id,
                     alt_status: 0
                     # subscription_id: subscription_id
        )
end

def pin_save2(session_id, msisdn,pin)
  PinTable.create(
                     session_id: session_id,
                     pin: pin_hash(pin),
                     msisdn: msisdn,
                     alt_status: 0
                     # subscription_id: subscription_id
        )
end

def subscriber_id
id = Time.new.strftime("%Y%m%d%H%M%S%L") # Get current date to the milliseconds
    id = id.to_i.to_s(36)
    
end

def pin_hash(pin)
    Digest::SHA256.hexdigest(pin)
end



def genUniqueCode
  time=Time.new
  strtm=time.strftime("%y%m%d%L%H%M")
  return strtm
end

def network_select(nw_code)
  _network = ""

  if nw_code == "01"
    _network = "MTN"
  elsif nw_code == "02"
    _network = "VOD"
  elsif nw_code == "03"
    _network = "TIG"
  elsif nw_code == "06"
    _network = "AIR"
  end

  return _network
end

def recipient_network(sessionID) 
  
  network = UssdLog.where('session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
  ussd_body = ""
  puts "---------------------------------------------------"
  puts "LETS SEE THE RECIPIENT NETWORK #{network}"
  puts "-----------------------------------------------------"

  if network == "1"
    ussd_body = "TIG"
  elsif network == "2"
    ussd_body = "MTN"
  elsif network == "3"
    ussd_body = "VOD"
  elsif network == "4"
    ussd_body = "AIR"
  end

  return ussd_body
end 

def v_recipient_network(sessionID) 
  
  network = UssdLog.where('session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
  ussd_body = ""
  puts "---------------------------------------------------"
  puts "LETS SEE THE RECIPIENT NETWORK #{network}"
  puts "-----------------------------------------------------"

  if network == "1"
    ussd_body = "TIG"
  elsif network == "2"
    ussd_body = "MTN"
  elsif network == "3"
    ussd_body = "VOD"
  elsif network == "4"
    ussd_body = "AIR"
  end

  return ussd_body
end 

def id_type(sessionID) 
  
 id_type= UssdLog.where('session_id=?  and sequence=?',sessionID, '7').order('id asc')[0].ussd_body 
  ussd_body = ""
  puts "---------------------------------------------------" 
  puts "LETS SEE THE ID TYPE #{id_type}"
  puts "-----------------------------------------------------"

  if id_type == "1"
    ussd_body = "Voters' ID"
  elsif id_type == "2"
    ussd_body = "Passport"
  elsif id_type == "3"
    ussd_body = "Drivers' License"
 
  end

  return ussd_body
end 

def id_type1(sessionID) 
  
 id_type= UssdLog.where('session_id=?  and sequence=?',sessionID, '6').order('id asc')[0].ussd_body 
  ussd_body = ""
  puts "---------------------------------------------------" 
  puts "LETS SEE THE ID TYPE #{id_type}"
  puts "-----------------------------------------------------"

  if id_type == "1"
    ussd_body = "Voters' ID"
  elsif id_type == "2"
    ussd_body = "Passport"
  elsif id_type == "3"
    ussd_body = "Drivers' License"
 
  end

  return ussd_body
end 

def sender_name(mobile) 
  
  first_name = Subscription.where('msisdn=?',mobile).order('id desc')[0].first_name
  last_name = Subscription.where('msisdn=?',mobile).order('id desc')[0].last_name
  sender_name = first_name + " " + last_name
  puts "---------------------------------------------------"
  # puts "LETS SEE THE SENDER NAME #{ sender_name}"
  puts "-----------------------------------------------------"

 

  return sender_name 
end    


def nwcode_select(network)
  _nwcode = ""

  if network == "MTN"
    _nwcode = "01"
  elsif network == "VOD"
    _nwcode = "02"
  elsif network == "TIG"
    _nwcode = "03"
  elsif network == "AIR"
    _nwcode = "06"
  end

  return _nwcode
end
 

def TranSaver(customer_number, amount, u_code, nw,recipient_number,recipient_telco,sender_name,voucher_code)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  TransactionSummary.create(mobile_number: customer_number, amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,recipient_number: recipient_number,recipient_telco: recipient_telco,sender_name: sender_name,voucher_code: voucher_code)
end

def ThirdParty(customer_number, amount, u_code, nw,recipient_number,sender_name,voucher_code)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  TVirtualTransaction.create(mobile_number: customer_number, amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,recipient_number: recipient_number,sender_name: sender_name,voucher_code: voucher_code)
end

def VirtualTrans(customer_number, amount, u_code, nw,voucher_code)
  time = Time.now.strftime('%Y-%m-%d %H:%M:%S')

  VirtualTransaction.create(mobile_number: customer_number, amount: amount,
                            ack_code: u_code, telco: nw,
                            status: '1', created_at: time,voucher_code: voucher_code)
end

######### Virtual Transfers Functions ##################
def virtualMobileMoneyReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
  
   VirtualTrans(customer_number,amount, u_code, nw,v_num)

ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw,
     :trans_type => trans_type,
     :callback_url => callback_url,
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end



def third_party_amfp(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
  
   ThirdParty(customer_number,amount, u_code, nw,recipient_number,sender_name,v_num)

ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw, 
     :trans_type => trans_type,
     :callback_url => callback_url, 
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end



def third_party_processPayment(ussd_body, sessionID, mobile, _msg_type,nw_code)
  u_code=genUniqueCode
  nw = network_select(nw_code)
 # recipient_telco = recipient_network(sessionID)
  sender_name =sender_name(mobile)
  v_num ="" 

  strConfirm = ussd_body.strip
  user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
  customer_mobile =  UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
  amount = UssdLog.where(' session_id=?  and sequence=?', sessionID, '7')[0].ussd_body 
 
 
  format_custom_number = phone_formatter(customer_mobile)
  puts
  puts "The  amount to deposit: #{amount}"
  puts
  puts "The recipient no: #{customer_mobile}"
         puts "FORMATED CUSTOMER NUMBER IS #{format_custom_number}"
  puts
  puts "The senders number no: #{user_phone}"
  puts
  puts "The sender's name is : #{sender_name}"
  


  if strConfirm == '1'
   
    old_amount = CustomerVirtualWallet.where(mobile_number: user_phone)[0] 
     rec_old_amount = CustomerVirtualWallet.where(mobile_number: format_custom_number)[0] 
     puts "Sender Virtual balance is #{old_amount.amount}"
      puts "Recipient OLd balance is #{rec_old_amount.amount}"
    
     if old_amount.amount == 0.00 
        strText3 = "Your virtual wallet balance is insufficient to deposit.Your current balance is #{old_amount.amount}.  Please top-up. "
      
      puts sendmsg(MSGSENDERID, user_phone, strText3)
     else
     
     new_amount = old_amount.amount - amount.to_f
     rec_new_amount = rec_old_amount.amount + amount.to_f
     
       puts "Sender NEW AMOUNT is #{new_amount}"
       puts "Recipient NEW AMOUNT is #{rec_new_amount}"
       
     

       
       check = CustomerVirtualWallet.where(:mobile_number=> format_custom_number ).exists?
       
       if check == true
     
     CustomerVirtualWallet.where(mobile_number: format_custom_number).update(:amount => rec_new_amount)
     CustomerVirtualWallet.where(mobile_number: user_phone).update(:amount => new_amount)
    
      strText2 = "You have successfully deposited GHC#{amount} to #{format_custom_number}.Your new virtual account balance is #{new_amount}. Your transaction id is: #{u_code}"
      strText4 = "You have received GHC#{amount} from #{sender_name} with number #{user_phone}.Your new virtual account balance is #{rec_new_amount}. The transaction id is: #{u_code}"
      
      puts sendmsg(MSGSENDERID, user_phone, strText2)
      puts sendmsg(MSGSENDERID, format_custom_number, strText4)
      strMainMenu2= 'Deposit Succesful!'
      else
        
        text = "Recipient Number is not registered on AlexPay."
        puts sendmsg(MSGSENDERID, user_phone, text)
        
        strMainMenu2= 'Recipient Number not registered on AlexPay.'
        
      end
  
     end

  elsif strConfirm == '0'
    puts 'Processing cancelled...' 
    strMainMenu2 = "Process Canceled"
  
  else
 
    strMainMenu2 = "Sorry wrong option"
  end

  strMainMenu2
end


def virtualprocessPayment(ussd_body, sessionID, mobile, _msg_type,nw_code)
  u_code=genUniqueCode
  nw = network_select(nw_code)
  recipient_telco = recipient_network(sessionID)
  sender_name =sender_name(mobile)
  v_num ="" 

  strConfirm = ussd_body.strip
  user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
  amount = UssdLog.where(' session_id=?  and sequence=?', sessionID, '6')[0].ussd_body 
 
  puts
  puts "The Deposit amount no: #{amount}"
  puts
  puts
  puts "The senders number no: #{user_phone}"
  puts
 
  
 

  if strConfirm == '1'
    puts 'Initiating AMFP request...'
    puts
     puts "The amount IS: #{amount}"
   if  balance_enough?(amount, nw)
     if nw_code == "01"
       
         puts "using MTN"
         Thread.new {
            virtualMobileMoneyReq(MTN_MERCHANT, user_phone, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num)
       }
        elsif nw_code == "02"
          
          puts "using vodafone"
          Thread.new {
       
            virtualMobileMoneyReq(VOD_MERCHANT, user_phone, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num)
        }
        elsif nw_code == "03" 
         
          puts "using tigo"
          Thread.new {
          virtualMobileMoneyReq(TIG_MERCHANT, user_phone, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num)
        }  
        elsif nw_code == "06"
         
          puts "using airtel"
          Thread.new {
           virtualMobileMoneyReq(MERCHANT_NO, user_phone, amount, STRREF, VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num)
        }
        end
   
     strMainMenu2= 'Transaction in process'
     
     else
       
       strMainMenu2= 'Insufficient Balance to transfer.'
     end

  elsif strConfirm == '0'
    puts 'Processing cancelled...' 
    strMainMenu2 = "Process Canceled"
  
  else
 
    strMainMenu2 = "Sorry wrong option"
  end

  strMainMenu2
end

def virtualprocessPayment2(ussd_body, sessionID, mobile, _msg_type,nw_code)
  u_code=genUniqueCode
  nw = network_select(nw_code)
  recipient_telco = recipient_network(sessionID)
  sender_name =sender_name(mobile)
  v_num ="" 

  strConfirm = ussd_body.strip
  user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
  amount = UssdLog.where(' session_id=?  and sequence=?', sessionID, '6')[0].ussd_body 
 
  puts
  puts "The amount to withdraw is: #{amount}"
  puts
  puts
  puts "The senders number no: #{user_phone}" 
  puts

  if strConfirm == '1'
    puts 'Initiating AMFP request...'
    puts
     puts "The amount IS: #{amount}"
   if  balance_enough?(amount, nw)
     if nw_code == "01"
       
         puts "using MTN"
         Thread.new {
            virtualMobileMoneyReq(MTN_MERCHANT, user_phone, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_CREDIT,v_num)
       }
        elsif nw_code == "02"
          
          puts "using vodafone"
          Thread.new {
       
            virtualMobileMoneyReq(VOD_MERCHANT, user_phone, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_CREDIT,v_num)
        }
        elsif nw_code == "03" 
        
          puts "using tigo"
          Thread.new {
          virtualMobileMoneyReq(TIG_MERCHANT, user_phone, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_CREDIT,v_num)
        }  
        elsif nw_code == "06"
         
          puts "using airtel"
          Thread.new {
           virtualMobileMoneyReq(MERCHANT_NO, user_phone, amount, STRREF, W_VIR_CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_CREDIT,v_num)
        }
        end
   
     strMainMenu2= 'Transaction in process'
     
     else
       
       strMainMenu2= 'Insufficient Balance to transfer.'
     end

  elsif strConfirm == '0'
    puts 'Processing cancelled...' 
    strMainMenu2 = "Process Canceled"
  
  else
 
    strMainMenu2 = "Sorry wrong option"
  end

  strMainMenu2
end




def self_dep_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
   

   strPreview= "Self Deposit Summary:\n"
   strPreview << "\n"
 
   strPreview << "User Number: #{user_phone}\n"
   strPreview << "Amount to deposit: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 
 
############## VIrtual Transactions fucntions end ########
def mobileMoneyReq(merchant_number, customer_number, amount, reference, callback_url, client_id, nw, trans_type,v_num,recipient_number,recipient_telco,sender_name)
 
  u_code=genUniqueCode
  
  url = 'https://184.173.139.74:8215'
  url_endpoint = '/sendRequest'

  
  conn = Faraday.new(url: url, headers: REQHDR, ssl: { verify: false }) do |f|
          f.response :logger
          f.adapter Faraday.default_adapter
  end
  
   TranSaver(customer_number,amount, u_code, nw,recipient_number,recipient_telco,sender_name,v_num)

ts=Time.now.strftime("%Y-%m-%d %H:%M:%S") 


puts
puts "Hello world..."
puts

payload={
     :customer_number => customer_number,
     :reference => STRREF,
     :amount => amount,
     :exttrid => u_code,
     :nw => nw, 
     :trans_type => trans_type,
     :callback_url => callback_url, 
     :ts => ts,
     :client_id => client_id,
     :voucher_code=> v_num
 }

puts
puts "Hello world Two..."
puts

json_payload=JSON.generate(payload)
msg_endpoint="#{json_payload}"

puts
puts "JSON payload: #{json_payload}"
puts

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
  
signature=computeSignature(SECRET_KEY, json_payload)

#begin
   res = conn.post do |req|
          req.url url_endpoint
          req.options.timeout = 30           # open/read timeout in seconds
          req.options.open_timeout = 30      # connection open timeout in seconds
          req['Authorization'] = "#{CLIENT_KEY}:#{signature}"
          req.body = json_payload
   end
        puts
        puts "Result from AMFP: #{res.body}"
        puts

end

def previewEntries(sessionID, mobile,nw_code)
  puts "-------------------" 
  puts "starting preview entries"
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
    nw = network_select(nw_code)
    recipient_telco = recipient_network(sessionID) 
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
    rec_number = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[1].ussd_body
    
    puts "lets see the number #{recipient_telco} and amount #{amount}"

   strPreview= "Funds Transfer(Network) Summary:\n"
   strPreview << "\n"
 
   strPreview << "Recipient Number: #{rec_number}\n"
   strPreview << "Recipient Network: #{recipient_telco}\n"
   strPreview << "Amount to transfer: GHC#{amount}\n"
   strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 

def previewEntries2(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
    recipient_telco = v_recipient_network(sessionID)
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '8').order('id desc')[1].ussd_body
    rec_number = UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[0].ussd_body
     voucher = UssdLog.where(' session_id=?  and sequence=?',sessionID, '8').order('id desc')[0].ussd_body
    

   strPreview= "Funds Transfer(Network) Summary:\n"
   strPreview << "\n"
 
   strPreview << "Recipient Number: #{rec_number}\n"
   strPreview << "Recipient Network: #{recipient_telco}\n"
   strPreview << "Amount to transfer: #{amount}\n" 
    strPreview << "Voucher code: #{voucher}\n"
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 



def vself_dep_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
   

   strPreview= "Self Deposit Summary:\n"
   strPreview << "\n"
 
   strPreview << "User Number: #{user_phone}\n"
   strPreview << "Amount to deposit: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 
def self_with_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
   

   strPreview= "Self Withdrawal Summary:\n"
   strPreview << "\n"
 
   strPreview << "User Number: #{user_phone}\n"
   strPreview << "Amount to withdraw: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 
def vself_with_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
   

   strPreview= "Self Withdrawal Summary:\n"
   strPreview << "\n"
 
   strPreview << "User Number: #{user_phone}\n"
   strPreview << "Amount to withdraw: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 
 
def thirdparty_dep_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
     rec_number = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[1].ussd_body
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
   

   strPreview= "Third-party Deposit Summary:\n"
   strPreview << "\n"
 
   strPreview << "Recipient Number: #{rec_number}\n"
   strPreview << "Amount to deposit: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 

def vthirdparty_dep_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
     rec_number = UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[1].ussd_body
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[0].ussd_body
   

   strPreview= "Third-party Deposit Summary:\n"
   strPreview << "\n"
 
   strPreview << "Recipient Number: #{rec_number}\n"
   strPreview << "Amount to deposit: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 

def thirdparty_withdrawal_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
     rec_number = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[1].ussd_body
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
   

   strPreview= "Third-party Withdrawal Summary:\n"
   strPreview << "\n"
 
   strPreview << "Recipient Number: #{rec_number}\n"
   strPreview << "Amount to wihtdrawl: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 

def vthirdparty_withdrawal_summary(sessionID, mobile,nw_code)
  
    user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
     rec_number = UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[1].ussd_body
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[0].ussd_body
   

   strPreview= "Third-party Withdrawal Summary:\n"
   strPreview << "\n"
 
   strPreview << "Recipient Number: #{rec_number}\n"
   strPreview << "Amount to wihtdrawl: #{amount}\n" 
    strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 

def previewSubscription(sessionID, mobile,nw_code)
  
    
    first_name = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
last_name= UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
alt_num= UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
id_type= id_type(sessionID) 
id_num= UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[0].ussd_body
    

   strPreview= "Subscription Details:\n"
   strPreview << "\n"
 
   strPreview << "First Name: #{first_name}\n"
   strPreview << "Last name: #{last_name}\n"
   strPreview << "Alternate Mobile Number: #{alt_num}\n"
   strPreview << "ID Type: #{id_type}\n"
   strPreview << "ID number: #{id_num}\n\n\n"
   strPreview << "\n"
   
  strPreview << "Confirm : \n"   
     
   strPreview << "1. Yes\n0. No"
  # strPreview << "1. Yes\n0. No"
end 

def previewSubscription_new(sessionID, mobile,nw_code)
  
    
    first_name = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
last_name= UssdLog.where(' session_id=?  and sequence=?',sessionID, '4').order('id desc')[0].ussd_body
#alt_num= UssdLog.where(' session_id=?  and sequence=?',sessionID, '5').order('id desc')[0].ussd_body
id_type= id_type1(sessionID) 
id_num= UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
    

   strPreview= "Subscription Details:\n"
   strPreview << "\n"
 
   strPreview << "First Name: #{first_name}\n"
   strPreview << "Last name: #{last_name}\n"
   #strPreview << "Alternate Mobile Number: #{alt_num}\n"
   strPreview << "ID Type: #{id_type}\n"
   strPreview << "ID number: #{id_num}\n"
   
  strPreview << "Confirm : \n"   
     
   strPreview << "1. Yes\n0. No"
  # strPreview << "1. Yes\n0. No"
end 


def deposit_summary(sessionID, mobile,nw_code) 
  
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
    puts "-----------------------"
    puts " LETS SEE THE AMOUNT TO DEPOSIT #{amount}"   
    puts "--------------------------------------" 

   strPreview= "Summary:\n"
   strPreview << "\n"
 
  
   strPreview << "Amount to deposit: GHC#{amount}\n"
   strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 

def withdraw_summary(sessionID, mobile,nw_code) 
  
   
    amount = UssdLog.where(' session_id=?  and sequence=?',sessionID, '3').order('id desc')[0].ussd_body
    puts "-----------------------"
    puts " LETS SEE THE AMOUNT TO WITDRAW #{amount}"   
    puts "--------------------------------------" 

   strPreview= "Summary:\n"
   strPreview << "\n"
 
  
   strPreview << "Amount to Withdraw: GHC#{amount}\n"
   strPreview << "Confirm Transaction: \n"  

   strPreview << "1. Yes\n0. No"
end 


def processPayment(ussd_body, sessionID, mobile, _msg_type,nw_code)
  u_code=genUniqueCode
  nw = network_select(nw_code)
  recipient_telco = recipient_network(sessionID)
  sender_name =sender_name(mobile)
  v_num ="" 

  strConfirm = ussd_body.strip
  user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
  customer_mobile =  UssdLog.where(' session_id=?  and sequence=?',sessionID, '6').order('id desc')[0].ussd_body
  amount = UssdLog.where(' session_id=?  and sequence=?', sessionID, '7')[0].ussd_body 
 
  puts
  puts "The client amount no: #{amount}"
  puts
  puts "The recipient no: #{customer_mobile}"
  puts
  puts "The senders number no: #{user_phone}"
  puts
  puts "The sender's name is : #{sender_name}"
  
 

  if strConfirm == '1'
    puts 'Initiating AMFP request...'
    puts
     puts "The amount IS: #{amount}"
   if  balance_enough?(amount, nw)
     if nw_code == "01"
       
         puts "using MTN"
         Thread.new {
            mobileMoneyReq(MTN_MERCHANT, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
       }
        elsif nw_code == "02"
          
          puts "using vodafone"
          Thread.new {
       
            mobileMoneyReq(VOD_MERCHANT, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
        }
        elsif nw_code == "03" 
        
          puts "using tigo"
          Thread.new {
          mobileMoneyReq(TIG_MERCHANT, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
        }  
        elsif nw_code == "06"
         
          puts "using airtel"
          Thread.new {
           mobileMoneyReq(MERCHANT_NO, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
        }
        end
   
     strMainMenu2= 'Transaction in process'
     
     else
       
       strMainMenu2= 'Insufficient Balance to transfer.'
     end

  elsif strConfirm == '0'
    puts 'Processing cancelled...' 
    strMainMenu2 = "Process Canceled"
  
  else
 
    strMainMenu2 = "Sorry wrong option"
  end

  strMainMenu2
end

def v_processPayment(ussd_body, sessionID, mobile, _msg_type,nw_code)
  u_code=genUniqueCode
  nw = network_select(nw_code)
  recipient_telco = v_recipient_network(sessionID)
  sender_name =sender_name(mobile)
 

  strConfirm = ussd_body.strip
  user_phone = UssdLog.where(' session_id=?', sessionID).select(:msisdn)[0][:msisdn]
  customer_mobile =  UssdLog.where(' session_id=?  and sequence=?',sessionID, '7').order('id desc')[0].ussd_body
  amount = UssdLog.where(' session_id=?  and sequence=?', sessionID, '8')[0].ussd_body 
 
  puts
  puts "The client amount: #{amount}"
  puts
  puts "The recipient no: #{customer_mobile}"
  puts
  puts "The senders number no: #{user_phone}"
  puts
  puts "The sender's name is : #{sender_name}"
 
  
 

  if strConfirm == '1'
    puts 'Initiating AMFP request...'
    puts
     puts "The amount IS: #{amount}"
   if  balance_enough?(amount, nw)
     if nw_code == "01"
       
         puts "using MTN"
        # Thread.new {
        v_num = "n/a"
            mobileMoneyReq(MTN_MERCHANT, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
       #}
        elsif nw_code == "02"
           v_num =UssdLog.where(' session_id=?  and sequence=?', sessionID, '9')[0].ussd_body
           puts "THis is the  Vocucher number in AMFP #{v_num}" 
          puts "using vodafone"
        #  Thread.new {
       
            mobileMoneyReq(VOD_MERCHANT, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
       # }
        elsif nw_code == "03" 
         v_num = "n/a"
          puts "using tigo"
         # Thread.new {
          mobileMoneyReq(TIG_MERCHANT, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
       # }  
        elsif nw_code == "06"
       v_num = "n/a"
          puts "using airtel"
        #  Thread.new {
           mobileMoneyReq(MERCHANT_NO, user_phone, amount, STRREF, CALLBACK_URL, CLIENT_ID, nw, TRANS_TYPE_DEBIT,v_num,customer_mobile,recipient_telco,sender_name)
       # }
        end
   
     strMainMenu2= 'Transaction in process'
     
     else
       
       strMainMenu2= 'Insufficient Balance to transfer.'
     end

  elsif strConfirm == '0'
    puts 'Processing cancelled...' 
    strMainMenu2 = "Process Canceled"
  
  else
 
    strMainMenu2 = "Sorry wrong option"
  end

  strMainMenu2
end


def sendmsg(senderID, recipient, txtmsg)
  strUser = 'tester'
  strPass = 'foobar'
  strIP = '184.173.139.74'
  strHostPort = '13013'

  time = Time.new
  msgID = time.strftime('%y%m%d%H%M%S%L')

  strDlrUrl = "http://#{strIP}:#{strHostPort}/Dlr?msgID=#{msgID}&smsc_reply=%A&sendsms_user=%n&ani=%P&bni=%p&dlr_val=%d&smsdate=%t&smscid=%i&charset=%C&mclass=%m&bmsg=%b&msg=%a"

  uri = URI("http://#{strIP}:#{strHostPort}/cgi-bin/sendsms")
  uri.query = URI.encode_www_form([['username', strUser], ['password', strPass], ['charset', 'UTF-8'], ['to', recipient], ['dlr-mask', '31'], ['from', senderID], ['text', txtmsg], %w(smsc airtel), ['dlr-url', strDlrUrl]])

  res = Net::HTTP.get_response(uri)
  puts res.code
  puts res.body
 
  p resp = Message.create!(msg_id: senderID, phone_num: recipient, msg: txtmsg, resp_code: res.code, resp_desc: res.body, status: 1)
end