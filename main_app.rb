require 'sinatra'
require 'json'
require 'nokogiri'
require 'sinatra/activerecord'
require 'yaml'
require 'faraday'
require 'net/http'
require 'uri'
require 'savon'
require 'digest/md5'
require 'openssl'
require './model'
require './functions'
require './callback'

######## CONSTANTS ###############
MAINMENU="Welcome to AlexPay \n\n 1.Mobile money\n 2.Transfer money\n 3.Merchant and Services\n 4.Bill Payments\n 5.Buy Airtime"
MOBMONEY = "1.Deposit Money\n\n 2.Withdraw Money"
TELCOSELECT="Select network \n\n1.Tigo\n2.MTN\n3.Vodafone\n4.Airtel"
VODMSG= "You need a voucher code in order to make payment. Please dial *110# and select 6 to generate a voucher code and return here to continue.\n\n1. Continue\n 2. Exit\n"
ENTERAMNT = "Please enter an amount to deposit"
MSGSENDERID = "alexPay"
STRTRANSFAILURE ="Sorry,your transaction failed"
MERCHANT_NUMBER = "261064828"
TRANS_TYPE_DEBIT ="DR"
TRANS_TYPE_CREDIT ="CR"
CALLBACK_URL = "http://67.205.74.208:8001/alex_pay_callback"
CLIENT_ID = 22
SECRET_KEY ='W0Dz7jXYrkS3QJr5RpnwZGjY8i5PxzuHXhzmiMfLdnM9BePz4RDRPoQibIJ4OLxIOD2UUpRTujcGfTA4dB3Qtw=='
CLIENT_KEY = 'XlnBgzaj/4ZRseV3AratiyjVJeR75Hbhy0wq3y2XIO2bPNZ6jAct5XIC70oovj3kJf4oygF6kcgIzAbbwc7kvw=='
MERCHANT_NO = "261064828"
TIG_MERCHANT = "0271300376"
MTN_MERCHANT = ""
VOD_MERCHANT = ""

post '/' do
  request.body.rewind
  @payload=JSON.parse(request.body.read)

  json_vals = @payload

  msg_type = json_vals['msg_type']
  sessionID=json_vals['session_id']
  service_key=json_vals['service_code']
  mobile_number=json_vals['msisdn']
  ussd_body=json_vals['ussd_body'].strip if json_vals.has_key?('ussd_body')
  nw_code = json_vals['nw_code']

  if msg_type == "0"
    sequence = "0"
  else
    sequence=get_sequence(sessionID,mobile_number);
  end

  log_book(msg_type,service_key,sessionID,sequence,mobile_number,ussd_body,nw_code)
  process(msg_type,service_key,sessionID,sequence,mobile_number,ussd_body,nw_code)

end

def log_book(msg_type,service_key,sessionID,sequence,mobile_number,ussd_body,nw_code)
  n = UssdLog.new
  n.msg_type = msg_type
  n.service_code = service_key
  n.session_id = sessionID
  n.msisdn = mobile_number
  n.ussd_body = ussd_body
  n.nw_code = nw_code
  n.sequence = sequence
  n.save
end

####### USSD PROCESS ###########

def process(msg_type, service_key, sessionID,sequence ,mobile, ussd_body,nw_code)
  #saveUSSD(msg_type, service_key, sessionID, sequence, mobile, ussd_body, nw_code)
  
  s_params=Hash.new

  s_params['msg_type']=msg_type
  s_params['session_id']=sessionID
  s_params['service_code']=service_key
  s_params['msisdn']=mobile
  s_params['nw_code']=nw_code

  if sequence == "0"

      s_params['msg_type']= '1'
      s_params['ussd_body']=MAINMENU

      p s_params.to_json

    
  elsif sequence=="1"
    
    strReply = UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '1').select(:ussd_body)[1][:ussd_body]
                                 
   puts "-------------------"
   puts "------StrREPLY is #{strReply}"
   puts "----------------------"   
      
      if strReply == "1"
       

      s_params['msg_type']= '1'
      s_params['ussd_body']= MOBMONEY
      
      elsif strReply == "2"
        
      s_params['msg_type']= '1'
      s_params['ussd_body']= TELCOSELECT
      
      elsif strReply == "3"
        
      s_params['msg_type']= '2'
      s_params['ussd_body']= "Sorry, Service not available yet"
      
      elsif strReply == "4"
        
      s_params['msg_type']= '2'
      s_params['ussd_body']= "Sorry, Service not available yet"      
      
      elsif strReply == "5"
        
     s_params['msg_type']= '2'
      s_params['ussd_body']= "Sorry, Service not available yet"
          
      end


  elsif sequence=="2"
    strReply = UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
    
                                 
   puts "-------------------"
   puts "------StrREPLY in sequence2 is #{strReply}"
  
   puts "----------------------"   
   
    if strReply == "1"
     if (strReply == "1" and ussd_body == "1") #deposit
       
      s_params['msg_type']= '1'
      s_params['ussd_body']= " Enter amount to deposit"
       
     elsif (strReply == "1" and ussd_body == "2") #withdraw
        s_params['msg_type']= '1'
      s_params['ussd_body']= " Enter amount to withdraw"
       
      end
     
   elsif strReply == "2" and
     
     #after network selection
       
      s_params['msg_type']= '1'
      s_params['ussd_body']= " Enter mobile number of recipient"
  
   end

  
  elsif sequence=="3"
    
    strReply = UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      prev_ussdbody =  UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]                         
   puts "-------------------"
   puts "------StrREPLY is #{strReply}"
    puts "------Previous ussd body in sequence 2 is #{prev_ussdbody}"
   puts "----------------------"   
   
   if (strReply == "1" and prev_ussdbody == "1")  #deposit summary
      s_params['msg_type']= '1'
      s_params['ussd_body']= deposit_summary(sessionID, mobile,nw_code)
    elsif (strReply == "1" and prev_ussdbody == "2" ) #wihtdraw summary
      s_params['msg_type']= '1'
      s_params['ussd_body']= withdraw_summary(sessionID, mobile,nw_code)
      
      
   elsif strReply == "2"
      s_params['msg_type']= '1'
      s_params['ussd_body']= " Enter amount to transfer"
     
   end

  
  elsif sequence == "4"
    
     strReply = UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]
      prev_ussdbody =  UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '4').select(:ussd_body)[0][:ussd_body]
     
                              
   puts "-------------------"
   puts "------StrREPLY is #{strReply}"
    puts "------Previous ussd body in sequence 2 is #{prev_ussdbody}"
   puts "----------------------" 
   
   if (strReply == "1" and prev_ussdbody == "1")  #deposit summary
     s_params["msg_type"] = '2'
      s_params['ussd_body']= processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
    elsif (strReply == "1" and prev_ussdbody == "1")  #deposit summary
      
      s_params["msg_type"] = '2'
      s_params['ussd_body']= processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
      
     elsif strReply == "2"
       
       s_params['msg_type']= '1'
      s_params['ussd_body']= previewEntries(sessionID, mobile,nw_code)
     
   end
   
  elsif sequence == '5'
    strReply = UssdLog.where('service_code=? and session_id=? and msisdn =? and sequence=?', service_key,
                                 sessionID, mobile, '2').select(:ussd_body)[1][:ussd_body]
    
    if strReply == "2"
      
      s_params["msg_type"] = '2'
      s_params['ussd_body']= processPayment(ussd_body, sessionID, mobile, sequence,nw_code)
  
    else
       s_params["msg_type"] = '2'
      s_params['ussd_body']= "An error occured"
    end
 

  end

  p s_params.to_json

end

